import numpy as np
import tifffile as tfl
import os
import shutil

imfolder = "./A"
altfolder = ["./A", "./B", "./OUT"]

mean = 0
std = 0

l = len(os.listdir(imfolder))
k = 0
for filename in os.listdir(imfolder):
    filepath = os.path.join(imfolder, filename)
    #with open(filepath, 'r') as f:
    ar = tfl.imread(filepath)
    s = np.mean(ar)
    if np.isnan(s):
        k += 1
        for fold in altfolder:
            old_filepath = os.path.join(fold, filename)
            new_filepath = os.path.join(fold + "-nans", filename)
            print("Moving {} to {}".format(old_filepath, new_filepath))
            shutil.move(old_filepath, new_filepath)
        continue
    mean += s
    d = np.std(ar)
    std += d
    print(s, d)

print(l, k)
l -= k
mean /= l
std /= l

print(mean, std)
