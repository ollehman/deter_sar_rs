from scipy.ndimage.filters import uniform_filter
from scipy.ndimage.measurements import variance
import numpy as np
import tifffile
from matplotlib import pyplot as plt
import os
from tqdm import tqdm
import scipy.ndimage
from PIL import Image

#lee filter on a single image
def lee_filter(img, in_channels=1):
    size = 224
    if in_channels == 1:
        input_shape = (size, size)
    else:
        input_shape = (size, size, in_channels)
    img_mean = uniform_filter(img, input_shape)
    img_sqr_mean = uniform_filter(img**2, input_shape)
    img_variance = img_sqr_mean - img_mean**2

    overall_variance = variance(img)

    img_weights = img_variance / (img_variance + overall_variance)
    img_output = img_mean + img_weights * (img - img_mean)
    return img_output


#lee filter on a directory:
# src should be the path to the source directory containing the sar images
# out should be the path to where you want to store the despeckled images
#conf is the configuration used: e.g. 1a, 2b etc.
def lee_filter_on_dir(src, out, conf):
    despeckle_last = 0
    if(conf == '1b' or conf == '2b' or conf == '3b'): despeckle_last = 1
    if not os.path.exists(out):
            os.makedirs(out)

    for filename in tqdm(os.listdir(src)):
        srctemp = src + "/" + filename
        if not srctemp[-4:] == ".tif":
            print(srctemp)
            continue
        im = tifffile.imread(srctemp)
      
        if("target" in filename):
            tifffile.imsave(out + "/" + filename, im)
        else:
            im = tifffile.imread(srctemp)
            im = np.array(im)
            if(im.ndim == 3):
                i = 0
                for i in range(0, len(im[0][0]) - despeckle_last):
                    #print(np.shape(im[i]))
                    col_mean = np.nanmean(im[:,:, i], axis=0)
                    inds = np.where(np.isnan(im[:,:, i]))
                    im[:,:, i][inds] = np.take(col_mean, inds[1]) 
                    im[:,:, i] = lee_filter(im[:,:,i])
                    #print(im[:,:, i])
                tifffile.imsave(out + "/" + filename, im)
            else:
                col_mean = np.nanmean(im, axis=0)
                inds = np.where(np.isnan(im))
                im[inds] = np.take(col_mean, inds[1])  
                im = lee_filter(im)
                tifffile.imsave(out + "/" + filename, im)



#some testing

configs = ["1A", "2A", "2B", "3A", "3B"]
root_path = "/cluster/work/igp_psr/data/AI4Good/project_3a/data"
#root_path = "siamese/input_CNN_model"
for config in configs:
    config_path = root_path + "/config_" + config
    for tvt in ["/train", "/val", "/test"]:
        set_path = config_path + tvt
        for conf in ["/A", "/B", "/OUT"]:
            in_path = set_path + conf
            out_path = config_path + "/despeckled" + tvt + conf
            if not os.path.exists(out_path):
                os.makedirs(out_path)
            try:
                lee_filter_on_dir(in_path, out_path, config.lower())
                print(f"despeckled images for path {in_path}")
            except RuntimeError as e:
                print(in_path)
                print(e)


"""im = tifffile.imread('siamese/input_CNN_model/config_3A/test/B/112.tif')
f, axarr = plt.subplots(3,3)
imarray = np.array(im)
before = imarray
col_mean = np.nanmean(imarray, axis=0)
inds = np.where(np.isnan(imarray))
imarray[inds] = np.take(col_mean, inds[1])
after = lee_filter(imarray)


axarr[0].imshow(before)
axarr[1].imshow(after)

#plt.show()
