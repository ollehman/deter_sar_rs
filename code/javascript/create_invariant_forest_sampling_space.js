/*------------------------DESCRIPTION-----------------------------------------

This code creates an image where only those pixels have values where rainforest was untouched
in 2019. This is checked using existing data of deforestation and NDVI threshold values.

INPUTS: 1) forest_2018_inpe: image of forest area from PRODES data
2) static_no_forest_mask + deter_may2020: deforestation masks (for 2019, 2020)

OUTPUT: Collection of 3000 untouched rainforest points with coordinates and the above described attributes 

------------------------CODE----------------------------------------------*/
// Forest mask from 2018 retrieved from INPE/ PRODES data
var forest_2018_inpe = ee.Image("users/juandb/PESQUISA_DETER/floresta_prodes_2018_compress")

// masks where deforestation alerts happened 
var static_no_forest_mask = ee.Image("users/juandb/DETER_SAR_ANCILLARY/no_forest_mask_2019_raster_edit_23jun20")
var deter_may2020 = ee.Image("users/juandb/DETER_SAR_ANCILLARY/deter_amz_public_2020Aug27_CR_raster")

//"BLA" is just a polygon shapefile of the brazilian legal amazon
var BLA = ee.FeatureCollection("users/juandb/PRODES2019/brazilian_legal_amazon");

// scale according to Sentinel-1 resolution
var generalScale=20

// NDVI threshold for untouched rainforest
var forest_median_ndvi_threshold=0.85

// remove the deforested areas 
var emersed_forest=forest_2018_inpe
                    .updateMask(static_no_forest_mask.unmask(0).not())
                    .updateMask(deter_may2020.gt(0).unmask(0).not())

// "erode" the remaining forest areas to be sure that the remaining area belongs to 
// the heart of the rainforest, use a kernel of 5000 m 
var kernel_forests=emersed_forest.unmask(0).focal_min(5000, 'circle', 'meters').selfMask()

// Select only green pixels, with NDVI higher than 0.85 threshold for vegetation
var forest_median_ndvi_threshold=0.85
var l8_sr1 = ee.ImageCollection("LANDSAT/LC08/C01/T1_SR")
var l8_sr2 = ee.ImageCollection("LANDSAT/LC08/C01/T2_SR")
var l8_sr=l8_sr1.merge(l8_sr2).filterDate('2019-01-01','2020-01-01')
// only use clear and cloud free pixels 
var l8_sr_masked=l8_sr.map(function(img){return img.updateMask(img.select(['pixel_qa']).eq(322))})
var l8_sr_masked_ndvi_median=l8_sr_masked.map(function(img){return img.normalizedDifference(['B5','B4'])
  .copyProperties(img,['system:time_start'])}).median()
  
// mask the kernel forest, where NDVI is below the defined threshold
var kernel_forests_green=kernel_forests.addBands(l8_sr_masked_ndvi_median.rename('median_NDVI')).updateMask(l8_sr_masked_ndvi_median.gt(forest_median_ndvi_threshold))
var scale=1000
var kernel_forests_green_bla=kernel_forests_green.select(0).reduceRegion(ee.Reducer.sum(),BLA,scale)

//-------------------------EXPORT---------------------------------------------------
Export.image.toAsset({
  image: kernel_forests_green,
  region: BLA,
  scale: generalScale,
  description: 'invariant_forest_sampling_image',
  assetId: 'DETER_SAR_EXP/invariant_forest_sampling_image',
  maxPixels: 216967828660
})

//-------------------------VISUALIZATION---------------------------------------------
print ("total kernel forest area",ee.Number(kernel_forests_green_bla.values().get(0)).multiply(scale^2))
Map.addLayer(kernel_forests_green,{bands:['b1'],palette:['green']},"kernel_forests_green")