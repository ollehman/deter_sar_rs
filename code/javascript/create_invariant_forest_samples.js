/*------------------------DESCRIPTION------------------------------------------

This code samples 3000 datapoints, where the rainforest is untouched.

INPUT: warnings_sampling_image: created in "create_invariant_sampling_space.js"
The image only has values in areas where rainforest has not been touched.
The image has two bands: b1 and median_NDVI
The b1 band is just 1 (for all pixels with forest, I think)

OUTPUT: Collection of 3000 forest data points with coordinates and the two attributes b1, median_NDVI

------------------------CODE----------------------------------------------*/

//"BLA" is just a polygon shapefile of the brazilian legal amazonvar 
var BLA = ee.FeatureCollection("users/juandb/PRODES2019/brazilian_legal_amazon");

// image created in "create_invariant_sampling_space.js"
var warnings_sampling_image = ee.Image('users/detersaree/invariant_forest_sampling_image_kernel_1km')

// scale is set to 20 m according to the resolution of Sentinel-1 imagery
var generalScale=20

// random sampling
var seed=12312
var n=3000
var samples=warnings_sampling_image.stratifiedSample({
  numPoints:n,
  classBand:'b1',
  region:BLA,
  seed:seed,
  scale:generalScale,
  geometries:true
  })

// This creates an export task that can be started manually at the right
// side of the editor (Tasks)
Export.table.toAsset(samples, 'samples_invariant_n'+n, 'DETER_SAR_EXP/samples_invariant_n'+n)


/*----------------------------Visualization---------------------------------------
This prints the input and outputs to the console and adds the according layers to the Map
----------------------------------------------------------------------------------*/
print("Warnings samplings image", warnings_sampling_image)
Map.addLayer(warnings_sampling_image, {}, 'Warnings sampling image')
print ("Samples",samples)
Map.addLayer(samples,{},'Samples')