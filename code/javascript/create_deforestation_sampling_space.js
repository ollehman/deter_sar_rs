/*------------------------DESCRIPTION--------------------------------------------------------------------
This code generates an image where only pixels have values where deforestation in 2019 occurred.

INPUT: "warnings":collection of polygons from mapbiomas alerts. 
Mapbiomas alertas collect alerts from DETERSAR, GLAD, and other systems that are based on publicly 
available RS imagery. Mapbiomas then acquires PlanetScope commercial satellite images (3x3m resolution) 
to validate the deforestation alerts. Documentation (http://alerta.mapbiomas.org/en/metodologia) 
of the alert system is relatively poor.
Each alert polygon has 49 attributes. Among the attributes is a detection date, a date before the 
deforestation and a date after the deforestation. 

OUTPUT: warnings_sampling_image
The image only has values in areas where deforestation alerts were issued in 2019. 
The image has six bands: detection_dt, before_dt, after_dt, NDVI_before, NDVI_after,constant
The constant band is just 1 (for all pixels with deforestation)

------------------------CODE--------------------------------------------------------------------------*/
//"BLA" is just a polygon shapefile of the brazilian legal amazon
var BLA = ee.FeatureCollection("users/juandb/PRODES2019/brazilian_legal_amazon");
var generalScale=20

// warning polygons downloaded from mapbiomas alerts
var warnings=ee.FeatureCollection('users/juandb/DETER_SAR_EXP/mapbiomas_alerts_allattribs_2019_AMAZONIA')

// set dates in consistent format
warnings=warnings.map(function(ft){
                        ft=ft.set('detection_dt',ee.Date(ft.get('DataDetec')).millis())
                        var date_before=ft.get('before_dt')
                        var date_detect=ft.get('detection_dt')
                        var dif=ee.Number(date_detect).subtract(ee.Number(date_before))
                        return ft.set('dif_detect',dif)
                      })

// Selects 2019 alerts, alerts > 1ha, have less than 4 months image interval, 
// date of forested images is before date of detection
var filtered_warnings=warnings
                      .filterMetadata('AnoDetec','equals',2019)
                      .filterMetadata('AreaHa','greater_than',1)
                      .filterMetadata('days_inter','less_than',121)
                      .filterMetadata('after_dt','not_equals',null)
                      .filterMetadata('before_dt','not_equals',null)
                      .filterMetadata('dif_detect','greater_than',0)
                      
// Thresholds to filter out points that do not belong to the deforested class according to the NDVI value
// NDVI is an optical remote sensing index for vegetation
var ndvi_forest_threshold=.82
var ndvi_nonforest_threshold=.69

// access optical satellite data for NDVI
var l8_sr1 = ee.ImageCollection("LANDSAT/LC08/C01/T1_SR")
var l8_sr2 = ee.ImageCollection("LANDSAT/LC08/C01/T2_SR")

// For each filtered warning polygon: apply the following function that creates a mask image
// with NDVI before and NDVI after, remove pixels where NDVI has not significantly dropped
// then mosaic all the small images together to one big mask with NDVI information
var mask_collection=ee.ImageCollection(filtered_warnings.map(function(ft){
  var date1=ee.Date(ft.get('before_dt'))
  var date2=ee.Date(ft.get('after_dt'))
  var l8_sr=l8_sr1.merge(l8_sr2)
  // Only use pixels from the landsat 8 optical product, with pixel_qa value of 322.
  // A value of 322 means that the pixels are clear and there are not clouds present.
  var l8_sr_masked=l8_sr.map(function(img){return img.updateMask(img.select(['pixel_qa']).eq(322))})
  // Compute the NDVI for all pixels in the Landsat collection
  var l8_sr_masked_ndvi=l8_sr_masked.map(function(img){return img.normalizedDifference(['B5','B4'])
    .copyProperties(img,['system:time_start'])
  })
  
  // compute median NDVI in 3-months period before the before_dt and after after_dt
  var ndvi_before=l8_sr_masked_ndvi.filterDate(date1.advance(-3,'months'),date1).median()
  var ndvi_after=l8_sr_masked_ndvi.filterDate(date2,date2.advance(3,'months')).median()
  
  // only use those pixels where the NDVI has significantly dropped (between before_dt and after_dt)
  var mask=ndvi_before.gte(ndvi_forest_threshold).multiply(ndvi_after.lt(ndvi_nonforest_threshold))
              .addBands(ndvi_before.rename("NDVI_before"))
              .addBands(ndvi_after.rename("NDVI_after"))
              .clip(ft)
  return mask
})).mosaic()

// this mask only has one band with 0 and 1, no NDVI information
var mask=ee.Image(0).blend(mask_collection.select(0)).focal_min(20,'circle', 'meters').selfMask()

// Rasterize filtered_warnings
var properties=['detection_dt','before_dt', 'after_dt']
var reducer=ee.Reducer.first().forEach(properties)

// create raster image of the filtered polygons and add NDVI bands from NDVI mask
var filtered_warnings_raster=filtered_warnings
                              .reduceToImage(properties,reducer)
                              .addBands(mask_collection.select(1,2))
                              .addBands(ee.Image(1))
                              .updateMask(mask)

// export the image with filtered warnings and bands for NDVI and dates before, after                              
Export.image.toAsset({
  image: filtered_warnings_raster,
  region: BLA,
  scale: generalScale,
  description: 'warnings_sampling_image',
  assetId: 'DETER_SAR_EXP/warnings_sampling_image',
  maxPixels: 216967828660
})