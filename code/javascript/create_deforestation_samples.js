/*------------------------DESCRIPTION------------------------------------------

This code samples 3000 datapoints, where deforestation occurred according to the 
mapbiomas alerts.

INPUT: warnings_sampling_image: created in "create_deforestation_sampling_space.js"
The image only has values in areas where deforestation alerts were issued. 
The image has six bands: detection_dt, before_dt, after_dt, NDVI_before, NDVI_after,constant
The constant band is just 1 (for all pixels with deforestation)

OUTPUT: Collection of 3000 deforestation points with coordinates and the six above described attributes 

------------------------CODE----------------------------------------------*/

//"BLA" is just a polygon shapefile of the brazilian legal amazon
var BLA = ee.FeatureCollection("users/juandb/PRODES2019/brazilian_legal_amazon");


// warnings_sampling_image is not publicy available on the Google Earth Engine
// var warnings_sampling_image = ee.Image('users/juandb/DETER_SAR_EXP/warnings_sampling_image')

// created an image myself and uploaded it to my assets (should be publicly accessible now)
var warnings_sampling_image = ee.Image('users/tabeadonauer/warnings_sampling_image')

// scale 20 m according to Sentinel-1 resolution
var generalScale=20

// random sampling
var seed=23532542
var n=3000
var samples=warnings_sampling_image.stratifiedSample({
  numPoints:n,
  classBand:'constant',
  region:BLA,
  seed:seed,
  scale:generalScale,
  geometries:true
  })

// This creates an export task that can be started manually on the right
// side of the editor (Tasks)
Export.table.toAsset(samples, 'samples_deforested_n'+n, 'DETER_SAR_EXP/samples_deforested_n'+n)


/*----------------------------Visualization---------------------------------------
This prints the input and outputs to the console and adds the according layers to the Map
----------------------------------------------------------------------------------*/

print("Warnings samplings image", warnings_sampling_image)
Map.addLayer(warnings_sampling_image, {}, 'Warnings sampling image')
print ("Samples",samples)
Map.addLayer(samples,{},'Samples')
