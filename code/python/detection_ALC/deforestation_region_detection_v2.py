# -*- coding: utf-8 -*-
"""
Created on Sun Dec  5 11:59:39 2021

@author: wenjie
"""


import pandas as pd
import numpy as np
import datetime
import tqdm
from dateutil.relativedelta import relativedelta
import time
import pickle as pk
from sklearn.metrics import f1_score
import os
from sklearn.preprocessing import MinMaxScaler

def save_data(data,f):
    file = open(f,'wb')
    pk.dump(data,file)
    
def load_data(f):
    file = open(f,'rb')
    data = pk.load(file)
    return data


def remove_None(dic):
    new_dic = {}
    for key in dic.keys():
        if dic[key] != None:
            new_dic[key] = dic[key]
    return new_dic

def pre_filtering(data):
    '''    
    1.Filters out very high or low angle values: to avoid the efect 
    of the incidence angle in mountainous areas.
    2.filter out observations with less than 20 observations
    and with negative values
    '''
    data = data.copy()
    data_keys = list(data.keys())
    print('pre-filtering')
    for key in tqdm.tqdm(data_keys):
        lia = data[key]['LIA']
        if lia <=30 or lia >= 45:
            data = data.drop(key,axis = 1)
            continue
        ts = data[key]['orig_f']
        if None in ts.values():
            ts = remove_None(ts)
        data[key]['orig_f'] = ts
        if len(ts) < 20:
            data = data.drop(key,axis = 1)
            continue
        #ts_arr = np.array(list(ts.values()))
    return data

def pre_filtering_v2(data):
    '''    
    remove nan
    '''
    data = data.copy()
    data_keys = list(data.keys())
    print('pre-filtering v2')
    for key in tqdm.tqdm(data_keys):
        ts = data[key]['orig_f']
        if None in ts.values():
            ts = remove_None(ts)
        data[key]['orig_f'] = ts
    return data

def pre_filtering_v3(data):
    '''    
    remove nan and normalization
    '''
    normalizer = MinMaxScaler()
    data = data.copy()
    data_keys = list(data.keys())
    print('pre-filtering v3')
    for key in tqdm.tqdm(data_keys):
        ts = data[key]['orig_f']
        if None in ts.values():
            ts = remove_None(ts)
        values = list(normalizer.fit_transform(np.array(list(ts.values())).reshape(-1,1)).flatten())
        new_ts = dict(zip(list(ts.keys()),values))
        data[key]['orig_f'] = ts
    return data

def filter_ts_by_date(ts,bf_dt):
    before_dt = datetime.date(int(bf_dt[:4]),int(bf_dt[5:7]),int(bf_dt[8:10]))
    new_ts = {}
    for key in ts.keys():
        date = key
        date = datetime.date(int(date[:4]),int(date[4:6]),int(date[6:8]))
        if date < before_dt:
            new_ts[key] = ts[key]
    return new_ts

def filter_ts_for_detection(ts,af_dt,detection_period=120):
    after_dt = datetime.date(int(af_dt[:4]),int(af_dt[5:7]),int(af_dt[8:10]))
    max_date = after_dt + relativedelta(days=detection_period)
    new_ts = {}
    for key in ts.keys():
        date = key
        date = datetime.date(int(date[:4]),int(date[4:6]),int(date[6:8]))
        if date >= after_dt and date < max_date:
            new_ts[key] = ts[key]
    return new_ts
     

def calc_stats(ts,before_dt,filt):
    if filt:
        ts = filter_ts_by_date(ts,before_dt)
    #print(len( df_train.iloc[:,i]['orig_f']))
    ts = pd.DataFrame(ts.values(),index=ts.keys())
    mean_db = np.mean(10*np.log10(ts))
    quantile_db = np.quantile(10*np.log10(ts),0.01)
    mean_quantile_diff = mean_db - quantile_db
    return mean_quantile_diff

def classify(data_frame,meandb_th,stddb_th,threshold_factor):
    n_confirmed = 0
    size = data_frame.shape[1]
    result = []
    for j in tqdm.tqdm(range(size)):
        ts_j = data_frame.iloc[:,j]['orig_f']
        before_dt = data_frame.iloc[:,j]['before_dt']
        new_ts_j = filter_ts_by_date(ts_j,before_dt)
        new_ts_j = pd.DataFrame(new_ts_j.values(),index=new_ts_j.keys())
        mean_db = np.mean(10*np.log10(new_ts_j))
    
        after_dt = data_frame.iloc[:,j]['after_dt']
        detection_ts = filter_ts_for_detection(ts_j,after_dt)
        detection_ts = pd.DataFrame(detection_ts.values(),index=detection_ts.keys())
        
        threshold = mean_db - meandb_th - stddb_th*threshold_factor
        warnings = 10*np.log10(detection_ts) < threshold
        if np.sum(warnings*1).values[0] > 0:
            n_confirmed += 1
            result.append(1)
        else:
            result.append(0)
    return n_confirmed,result

def train_test(df_train,f_train,df_test,f_test,threshold_factor = 1):
    df_train_stats = []
    df_train_size = df_train.shape[1]
    print('training')
    for i in tqdm.tqdm(range(df_train_size)):
        ts_i = df_train.iloc[:,i]['orig_f']
        before_dt = df_train.iloc[:,i]['before_dt']
        mean_quantile_diff_i = calc_stats(ts_i,before_dt,filt=True)
        df_train_stats.append(mean_quantile_diff_i.values[0])
    f_train_stats = []
    f_train_size = f_train.shape[1]
    for i in tqdm.tqdm(range(f_train_size)):
        ts_i = f_train.iloc[:,i]['orig_f']
        before_dt = f_train.iloc[:,i]['before_dt']
        mean_quantile_diff_i = calc_stats(ts_i,before_dt,filt=False)
        f_train_stats.append(mean_quantile_diff_i.values[0])
    train_stats = df_train_stats + f_train_stats
    train_stats = np.array(train_stats)   
    meandb_th = np.mean(train_stats)
    stddb_th = np.std(train_stats)
    
    print('testing')
    
    n_f = f_test.shape[1]
    n_confirmed_f, result_f = classify(f_test,meandb_th,stddb_th,threshold_factor) #number of ts classified to deforested one

    n_df = df_test.shape[1]
    n_confirmed_df, result_df = classify(df_test,meandb_th,stddb_th,threshold_factor)

    TP = n_confirmed_df
    TN = n_f - n_confirmed_f
    FP = n_confirmed_f
    FN = n_df - n_confirmed_df
    TS = n_f + n_df
    ACC = (TP+TN)/TS
    TNR = TN/(TN+FP)
    TPR = TP/(TP+FN)
    F1 = 2*TP/(2*TP+FP+FN)
    num_list = [n_f,n_confirmed_f,n_df,n_confirmed_df]
    score = np.array([TNR,TPR,F1,ACC])
    return num_list,score, result_f,result_df

def cross_val(df_train,f_train,thresholds):
    print('cross validation')
    avg_score = []
    df_size = df_train.shape[1]
    f_size = f_train.shape[1]
    shuffled_df_keys = np.random.permutation(df_train.columns)
    shuffled_f_keys = np.random.permutation(f_train.columns)
    
    df_train_fold1_keys = shuffled_df_keys[:(df_size)//2]
    df_train_fold2_keys = shuffled_df_keys[(df_size)//2:]
    f_train_fold1_keys = shuffled_f_keys[:(f_size)//2]
    f_train_fold2_keys = shuffled_f_keys[(f_size)//2:]

    df_train_fold1 = df_train[df_train_fold1_keys]
    df_train_fold2 = df_train[df_train_fold2_keys]    
    f_train_fold1 = f_train[f_train_fold1_keys]
    f_train_fold2 = f_train[f_train_fold2_keys]    
    for thres in list(thresholds):      
        _, score_1, _, _ = train_test(df_train_fold1,f_train_fold1,df_train_fold2,f_train_fold2,threshold_factor = thres)
        _, score_2, _, _ = train_test(df_train_fold2,f_train_fold2,df_train_fold1,f_train_fold1,threshold_factor = thres)        
        avg_score.append((score_1+score_2)/2)
    return thresholds,avg_score
        
def save_load_data(LOAD = True):
    if not LOAD:
        #img1
        # file_idx_1 = list(np.arange(1000,51000,1000))+[50177]
        # data1 = pd.DataFrame()
        # for idx in file_idx_1:
        #     path = './data/img1/4months_detect_' + str(idx)+'.json'
        #     try:
        #         tmp = pd.read_json(path)
        #         data1 = pd.concat([data1,tmp],axis=1)
        #     except:
        #         print(path + "not exists.")
        # #save data
        # save_data(data1, './data/img1/all_json.pkl')
        
        #img2
        # file_idx_1 = list(np.arange(2000,50000,2000))+[50176]
        # data2 = pd.DataFrame()
        # for idx in file_idx_1:
        #     path = './data/img2/4months_detect_' + str(idx)+'.json'
        #     try:
        #         tmp = pd.read_json(path)
        #         data2 = pd.concat([data2,tmp],axis=1)
        #     except:
        #         print(path + "not exists.")
        # #save data
        # save_data(data2, './data/img2/img2_all_json.pkl')
        
        #img3
        file_idx_1 = list(np.arange(2000,50000,2000))+[50176]
        data3 = pd.DataFrame()
        for idx in file_idx_1:
            path = './data/img3/id_950804months_detect_' + str(idx)+'.json'
            try:
                tmp = pd.read_json(path)
                data3 = pd.concat([data3,tmp],axis=1)
            except:
                print(path + "not exists.")
        #save data
        save_data(data3, './data/img3/img3_all_json.pkl')
        
    
    else:
        #load data
        data1 = load_data('./data/img1/img1_all_json.pkl')
        #data2 = load_data('./data/img2/img2_all_json.pkl')
        data3 = load_data('./data/img3/img3_all_json.pkl')
    return data1,data3


def get_defo_forest(data):
    print('get deforested/forest data')
    defo_idx = []
    forest_idx = []
    
    defo_num = 0
    forest_num = 0
    for col in tqdm.tqdm(data.columns):
        if data[col]['deforested']:
            #defo = pd.concat([defo,data[col]],axis=1)
            defo_idx.append(col)
            defo_num += 1
        else:
            #forest = pd.concat([forest,data[col]],axis = 1)
            forest_idx.append(col)
            forest_num += 1
    return data[defo_idx],data[forest_idx]

def get_csv_map(data,result_dict,img):
    true_mat = np.zeros((data.shape[1],3))
    pred_mat = np.zeros((data.shape[1],3))
    for key in result_dict.keys():
        true_label = data[key]['deforested']*1
        pred_label = result_dict[key]
        true_mat[key,0] = data[key]['cooridnates'][0]
        true_mat[key,1] = data[key]['cooridnates'][1]
        true_mat[key,2] = true_label
        pred_mat[key,0] = data[key]['cooridnates'][0]
        pred_mat[key,1] = data[key]['cooridnates'][1]
        pred_mat[key,2] = pred_label    
    true_df = pd.DataFrame(true_mat,columns=['cor_x','cor_y','deforested'])    
    pred_df = pd.DataFrame(pred_mat,columns=['cor_x','cor_y','deforested'])   
    true_df.to_csv(img + '_target_map_thres1.66.csv',index=False)
    pred_df.to_csv(img + '_prediction_map_thres1.66.csv',index=False)

st = time.time()

LOAD = True
data1,data2 = save_load_data(LOAD)
defo1,forest1 = get_defo_forest(data1)
defo2,forest2 = get_defo_forest(data2)


#--------------------pre filtering v3-----------------------------------------

v3 = False

if v3:
    defo_filtered1 = pre_filtering_v3(defo1)
    forest_filtered1 = pre_filtering_v3(forest1)
    defo_filtered2 = pre_filtering_v3(defo2)
    forest_filtered2 = pre_filtering_v3(forest2)
else:
    defo_filtered1 = pre_filtering_v2(defo1)
    forest_filtered1 = pre_filtering_v2(forest1)
    defo_filtered2 = pre_filtering_v2(defo2)
    forest_filtered2 = pre_filtering_v2(forest2)    
    
np.random.seed(0)

image_test_id = 1

if image_test_id == 1:
    defo_train = defo_filtered2
    forest_train = forest_filtered2
    defo_test = defo_filtered1
    forest_test = forest_filtered1
else:
    defo_train = defo_filtered1
    forest_train = forest_filtered1
    defo_test = defo_filtered2
    forest_test = forest_filtered2    

#simple train and test
num_list0,score_0, result_f,result_df = train_test(defo_train,forest_train,defo_test,forest_test,1.66)

# # cross-validation

# st = time.time()

# thresholds = [8] #np.arange(8,8,1)
# thresholds,avg_score = cross_val(defo_train, forest_train,thresholds)

# avg_score = np.array(avg_score)

# best_F1_threshold =  thresholds[np.argmax(avg_score[:,2])]
# print('train and test with best threshold')

# num_list3, score_3, result_f,result_df = train_test(defo_train,forest_train,defo_test,forest_test,best_F1_threshold)

defo_idx = list(defo_test.columns)
forest_idx = list(forest_test.columns)
defo_zip = zip(defo_idx,result_df)
forest_zip = zip(forest_idx,result_f)
defo_dict = dict(defo_zip)
forest_dict = dict(forest_zip)

forest_dict.update(defo_dict)
result_dict = forest_dict

if image_test_id == 1:
    get_csv_map(data1,result_dict,'img1')
else:
    get_csv_map(data2,result_dict,'img3')

end = time.time()

print("time elapsed: %.3f" %(end-st))

# #fine-tune 
# thresholds = np.arange(0,10,1)

# num_list = []
# score_list = []
# result_f_list = []
# result_df_list = []
# for thres in list(thresholds):
#     num, score, result_f,result_df = train_test(defo_train,forest_train,defo_test,forest_test,thres)
#     num_list.append(num)
#     score_list.append(score)
#     result_f_list.append(result_f)
#     result_df_list.append(result_df)


