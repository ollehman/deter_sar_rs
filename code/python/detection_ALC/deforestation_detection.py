# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 10:27:07 2021

@author: wenjie
"""

import pandas as pd
import numpy as np
import datetime
from dateutil.relativedelta import relativedelta


def remove_None(dic):
    new_dic = {}
    for key in dic.keys():
        if dic[key] != None:
            new_dic[key] = dic[key]
    return new_dic

def pre_filtering(data):
    '''    
    1.Filters out very high or low angle values: to avoid the efect 
    of the incidence angle in mountainous areas.
    2.filter out observations with less than 20 observations
    and with negative values
    '''
    data_keys = list(data.keys())
    for key in data_keys:
        lia = data[key]['LIA']
        if lia <=30 or lia >= 45:
            data = data.drop(key,axis = 1)
            continue
        ts = data[key]['orig_f']
        if None in ts.values():
            ts = remove_None(ts)
        data[key]['orig_f'] = ts
        if len(ts) < 20:
            data = data.drop(key,axis = 1)
            continue
        ts_arr = np.array(list(ts.values()))
    return data

def filter_ts_by_date(ts,bf_dt):
    before_dt = datetime.date(int(bf_dt[:4]),int(bf_dt[5:7]),int(bf_dt[8:10]))
    new_ts = {}
    for key in ts.keys():
        date = key
        date = datetime.date(int(date[:4]),int(date[4:6]),int(date[6:8]))
        if date < before_dt:
            new_ts[key] = ts[key]
    return new_ts

def filter_ts_for_detection(ts,af_dt,detection_period=120):
    after_dt = datetime.date(int(af_dt[:4]),int(af_dt[5:7]),int(af_dt[8:10]))
    max_date = after_dt + relativedelta(days=detection_period)
    new_ts = {}
    for key in ts.keys():
        date = key
        date = datetime.date(int(date[:4]),int(date[4:6]),int(date[6:8]))
        if date >= after_dt and date < max_date:
            new_ts[key] = ts[key]
    return new_ts
     

def calc_stats(ts,before_dt,filt):
    if filt:
        ts = filter_ts_by_date(ts,before_dt)
    #print(len( df_train.iloc[:,i]['orig_f']))
    ts = pd.DataFrame(ts.values(),index=ts.keys())
    mean_db = np.mean(10*np.log10(ts))
    quantile_db = np.quantile(10*np.log10(ts),0.01)
    mean_quantile_diff = mean_db - quantile_db
    return mean_quantile_diff

def classify(data_frame,meandb_th,stddb_th,threshold_factor):
    n_confirmed = 0
    size = data_frame.shape[1]
    for j in range(size):
        ts_j = data_frame.iloc[:,j]['orig_f']
        before_dt = data_frame.iloc[:,j]['before_dt']
        new_ts_j = filter_ts_by_date(ts_j,before_dt)
        new_ts_j = pd.DataFrame(new_ts_j.values(),index=new_ts_j.keys())
        mean_db = np.mean(10*np.log10(new_ts_j))
    
        after_dt = data_frame.iloc[:,j]['after_dt']
        detection_ts = filter_ts_for_detection(ts_j,after_dt)
        detection_ts = pd.DataFrame(detection_ts.values(),index=detection_ts.keys())
        
        threshold = mean_db - meandb_th - stddb_th*threshold_factor
        warnings = 10*np.log10(detection_ts) < threshold
        if np.sum(warnings*1).values[0] > 0:
            n_confirmed += 1

    return n_confirmed

def train_test(df_train,f_train,df_test,f_test,threshold_factor = 1):
    df_train_stats = []
    df_train_size = df_train.shape[1]
    for i in range(df_train_size):
        ts_i = df_train.iloc[:,i]['orig_f']
        before_dt = df_train.iloc[:,i]['before_dt']
        mean_quantile_diff_i = calc_stats(ts_i,before_dt,filt=True)
        df_train_stats.append(mean_quantile_diff_i.values[0])
    f_train_stats = []
    f_train_size = f_train.shape[1]
    for i in range(f_train_size):
        ts_i = f_train.iloc[:,i]['orig_f']
        before_dt = f_train.iloc[:,i]['before_dt']
        mean_quantile_diff_i = calc_stats(ts_i,before_dt,filt=False)
        f_train_stats.append(mean_quantile_diff_i.values[0])
    train_stats = df_train_stats + f_train_stats
    train_stats = np.array(train_stats)   
    meandb_th = np.mean(train_stats)
    stddb_th = np.std(train_stats)
    
    n_f = f_test.shape[1]
    n_confirmed_f = classify(f_test,meandb_th,stddb_th,threshold_factor) #number of ts classified to deforested one

    n_df = df_test.shape[1]
    n_confirmed_df = classify(df_test,meandb_th,stddb_th,threshold_factor)

    TP = n_confirmed_df
    TN = n_f - n_confirmed_f
    FP = n_confirmed_f
    FN = n_df - n_confirmed_df
    TS = n_f + n_df
    ACC = (TP+TN)/TS
    return ACC
defo = pd.read_json('deforested_results.json')
forest = pd.read_json('forest_results.json')

defo_filtered = pre_filtering(defo)
forest_filtered = pre_filtering(forest)
np.random.seed(0)


shuffled_defo_keys = np.random.permutation(defo_filtered.columns)
shuffled_forest_keys = np.random.permutation(forest_filtered.columns)

#ceate train and test set
defo_size = shuffled_defo_keys.shape[0]
defo_train_idx = shuffled_defo_keys[:(defo_size//2)]
defo_test_idx = shuffled_defo_keys[(defo_size//2):]
defo_train = defo_filtered[defo_train_idx]
defo_test = defo_filtered[defo_test_idx]

forest_size = shuffled_forest_keys.shape[0]
forest_train_idx = shuffled_forest_keys[:(forest_size//2)]
forest_test_idx = shuffled_forest_keys[(forest_size//2):]
forest_train = forest_filtered[forest_train_idx]
forest_test = forest_filtered[forest_test_idx]

acc = train_test(defo_train,forest_train,defo_test,forest_test)