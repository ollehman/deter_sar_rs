# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 11:44:48 2020

@author: juand
"""
import csv
import json
import datetime
import ee
import sys
from get_time_series_stab_harmonic_spatialmean_filter import get_point_timeseries_2stab_filter
from utils import get_timestamps_from_name_string, format_data

USAGE = "Usage: python {} point_asset_id is_forest learning_period band output_prefix init_point end_point".format(sys.argv[0])

args = sys.argv[1:]
if not args:
    raise SystemExit(USAGE)
if len(args)<5:
    print(USAGE)

include_S1B=0  # whether to include S1B sattelite data or not
points_collection=args[0]
is_forest=int(args[1])  # invariant forest or deforested
learningPeriodYears=int(args[2])  # 1 or 2 years
pol=args[3]  # polarization VH or VV?
output_prefix=args[4]  # where the data should be saved

extract_orig_meteo = False
evaluate = True

# test point 
ee.Initialize()
time_start=datetime.datetime.now()
npontos=ee.FeatureCollection(points_collection).size().getInfo()
if len(args)>5:
    init_point=int(args[5])
else:
    init_point=0
if len(args)==7:
    end_point=int(args[6])
else:
    end_point=npontos

# Adds a random column to invariant forest points and sets the output name of the resulting csv file
if is_forest: 
    lista_pontos=ee.FeatureCollection(points_collection).randomColumn().toList(npontos)
    #output_name = output_prefix + "_forestinvariant_" + str(learningPeriodYears) + "_" + pol + ".csv"
    output_name = output_prefix + "forest_results"
else:
    lista_pontos = ee.FeatureCollection(points_collection).toList(npontos)
    if evaluate:
        output_name = output_prefix + "4months_detect"
    else:
        output_name = output_prefix + "deforested_results"

#### EXTRACTION BEGINS HERE ####    
print ("Computing stabilized {} TS with {}yr period".format(pol, learningPeriodYears))
output = {}

for j in range(init_point,end_point):
    if j == npontos:
        break
    point = ee.Feature(lista_pontos.get(j))
    print (f"Point {j}")
    if is_forest:
        # generate a random deforestation date because point has no attribute "before_dt"
        ee_begin_detect_date=ee.Date('2019-01-01').advance(ee.Number(point.get('random')).multiply(365),'days')
    else:
        ee_begin_detect_date=ee.Date(point.get('before_dt'))
    
    if extract_orig_meteo:
        original, original_f, meteo_12h, meteo_60d= get_point_timeseries_2stab_filter(
            pol, point.geometry(), ee_begin_detect_date, \
            learningPeriodYears=learningPeriodYears, \
            detectPeriodMonths=12, \
            extract_orig_meteo=extract_orig_meteo)
    
        data = [
            [j, "orig"] + list(original.getInfo().values()),
            [j, "orig_f"] + list(original_f.getInfo().values()),
            [j, "12h"] + list(meteo_12h.getInfo().values()),
            [j, "60d"] + list(meteo_60d.getInfo().values())
        ]
        file = open('results.csv', 'a', newline ='')
        # writing the data into the file
        with file:    
            write = csv.writer(file)
            write.writerows(data)
        
    else:
        LIA, original_f, p_12h_ts, p_60d_ts = get_point_timeseries_2stab_filter(
            pol, 
            point.geometry(), 
            ee_begin_detect_date,
            learningPeriodYears=learningPeriodYears,
            detectPeriodMonths=4
            )

        # Time series Docu:
        # original_f  = original TS filtered for speckle noise
        # p_12h_ts = 12h precipitation TS
        # p_60d_ts = 60d precipitation TS
        if original_f is not None:
            orig_f_data=original_f.getInfo()
            #p_12h_data=p_12h_ts.getInfo()
            #p_60d_data=p_60d_ts.getInfo()
            datapoint_names=list(orig_f_data.keys())
            orig_f_values=list(orig_f_data.values())
            #p_12h_values=list(p_12h_data.values())
            #p_60d_values=list(p_60d_data.values())
            if evaluate:    
                deforested = bool(point.get('constant').getInfo())
                data = format_data(j, LIA, point, ee_begin_detect_date, datapoint_names, orig_f_values, not deforested)
            else:
                data = format_data(j, LIA, point, ee_begin_detect_date, datapoint_names, orig_f_values, is_forest)
            output[j] = data

# Save to json file
output_name = output_name + "_{}.json".format(j+1)
with open(output_name, 'w+', encoding='utf-8') as file:
    json.dump(output, file, indent=4)

time_end=datetime.datetime.now()
print ("elapsed time ",time_end-time_start)
