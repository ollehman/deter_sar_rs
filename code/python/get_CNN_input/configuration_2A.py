# LIBRARIES
from __future__ import print_function
import ee
import numpy as np
import pandas as pd

# INITIALIZE EARTH ENGINE
ee.Initialize()

# IMPORTS
sentinel1 = ee.ImageCollection('COPERNICUS/S1_GRD')
warnings = ee.FeatureCollection('users/juandb/DETER_SAR_EXP/mapbiomas_alerts_allattribs_2019_AMAZONIA')
id_series = pd.read_csv('warnings_with_random.csv')['id']

# FUNCTIONS
# create SQUARE bounding box
def makeRectangle(point, xDistance, yDistance, x_disp, y_disp, proj):
    geometry = ee.Geometry.Rectangle(
        [ee.Number(point.get(0)).subtract(xDistance).add(x_disp),
         ee.Number(point.get(1)).subtract(yDistance).add(y_disp),
         ee.Number(point.get(0)).add(xDistance).add(x_disp),
         ee.Number(point.get(1)).add(yDistance).add(y_disp)], proj, False)
    return geometry

# set detection date in milliseconds for mapbiomas features
def set_data_detec_ms(ft):
    ft = ft.set('detection_dt_ms', ee.Date(ft.get('DataDetec')).millis())
    return ft


# Sample around 1000 random polygons from mapbiomas
warnings = warnings.map(set_data_detec_ms).randomColumn(seed=131347)
warnings= warnings.filterMetadata('AnoDetec', 'equals', 2019)\
    .filterMetadata('detection_dt_ms','not_equals', None)\
    .sort('random')

# create a list to loop through
warnings_list = warnings.toList(10000)

# export loop
for n in range(0, 1000):
    print(n)
    # define random seed
    np.random.seed(n)

    try:
        feature = ee.Feature(warnings_list.get(n))
        # get centroid of polygon
        centroid = feature.geometry().centroid()
        id = str(id_series[n])

        # # SENTINEL IMAGES - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - -
        start_date = '2018-12-01'
        end_date = '2020-01-30'
        detection_dt = ee.Date(feature.get('DataDetec'))

        # Last image before detection
        img_before = sentinel1.filterDate(start_date, detection_dt.advance(-1, 'month'))\
            .filterBounds(centroid)\
            .limit(1, 'system:time_start', False).first()\
            .select('VH')

        # First sentinel image after detection
        img_after = sentinel1\
            .filterDate(detection_dt.advance(1, 'month'), end_date)\
            .filterBounds(centroid).first()\
            .select('VH')

        # Define SQUARE bounding box around centroid point
        # add random x,y displacement in order not to bias the network to find deforestation in the center
        scale = 10
        projection = img_before.projection().crs()
        point = centroid.transform(projection).coordinates()
        centroid_displacement_x = 300*np.random.randn()
        centroid_displacement_y = 300*np.random.randn()
        xDistance = ee.Number(112).multiply(scale)
        yDistance = ee.Number(112).multiply(scale)
        rect = makeRectangle(point, xDistance, yDistance, centroid_displacement_x, centroid_displacement_y, projection)

        # CROP SAR image with bounding box
        img_before_clip = img_before.clip(rect)
        img_after_clip = img_after.clip(rect)
        mask_image = img_before_clip.where(img_before_clip.lte(10), 0)

        # generate target image
        fc = warnings.filterBounds(rect)\
            .filterMetadata('detection_dt_ms', 'greater_than', img_before.date().millis())\
            .filterMetadata('detection_dt_ms', 'less_than', img_after.date().millis())

        fc_image = ee.Image.constant(0).clip(fc.geometry()).mask()
        target_image = mask_image.where(fc_image.eq(1), 1)

        # EXPORT
        task1=ee.batch.Export.image.toDrive(image=img_before, description= id,
                                            dimensions='224x224', region= rect,
                                            folder='config_2A_A')
        task1.start()

        task2 = ee.batch.Export.image.toDrive(image=img_after, description=id,
                                              dimensions='224x224', region=rect,
                                              folder='config_2A_B')
        task2.start()

        # task3 = ee.batch.Export.image.toDrive(image=target_image, description=id,
        #                                       dimensions= '224x224',region= rect,
        #                                       folder='config_2A_OUT')
        # task3.start()


    except BaseException as error:
            print('An exception occurred: {}'.format(error))
            continue
