# IMPORTS
from __future__ import print_function
import ee
import numpy as np

ee.Initialize()

# IMPORTS
sentinel1 = ee.ImageCollection('COPERNICUS/S1_GRD')
warnings = ee.FeatureCollection('users/juandb/DETER_SAR_EXP/mapbiomas_alerts_allattribs_2019_AMAZONIA')

# Forest mask from 2018 retrieved from INPE/ PRODES data
forest_2018_inpe = ee.Image("users/juandb/PESQUISA_DETER/floresta_prodes_2018_compress")

# masks where deforestation alerts happened
static_no_forest_mask = ee.Image("users/juandb/DETER_SAR_ANCILLARY/no_forest_mask_2019_raster_edit_23jun20")
deter_may2020 = ee.Image("users/juandb/DETER_SAR_ANCILLARY/deter_amz_public_2020Aug27_CR_raster")

# "BLA" is just a polygon shapefile of the brazilian legal amazon
BLA = ee.FeatureCollection("users/juandb/PRODES2019/brazilian_legal_amazon");

# FUNCTIONS
# create SQUARE bounding box
def makeRectangle(point, xDistance, yDistance, x_disp, y_disp, proj):
    geometry = ee.Geometry.Rectangle(
        [ee.Number(point.get(0)).subtract(xDistance).add(x_disp),
         ee.Number(point.get(1)).subtract(yDistance).add(y_disp),
         ee.Number(point.get(0)).add(xDistance).add(x_disp),
         ee.Number(point.get(1)).add(yDistance).add(y_disp)], proj, False)
    return geometry

# set detection date in milliseconds for mapbiomas features
def set_data_detec_ms(ft):
    ft = ft.set('detection_dt_ms', ee.Date(ft.get('DataDetec')).millis())
    return ft


# 1) MAPBIOMAS
warnings = warnings.map(set_data_detec_ms).filterMetadata('AnoDetec', 'equals', 2019)\

# make list
warnings_list = warnings.toList(1000)

for n in range(31, 1000):
    print(n)

    try:
        feature = ee.Feature(warnings_list.get(n))

        # get centroid of polygon
        centroid = feature.geometry().centroid()
        id = str(feature.get('id').getInfo())
        # # SENTINEL IMAGES - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - -
        start_date = '2018-12-15';
        end_date = '2020-01-15';
        detection_dt = ee.Date(feature.get('DataDetec'))

        # Last image before detection
        img_before = sentinel1.filterDate(start_date, detection_dt.advance(-1, 'day'))\
            .filterBounds(centroid)\
            .limit(1, 'system:time_start', False).first()\
            .select('VH')

        date_SAR_before = img_before.date().format('YYYY-MM-dd').getInfo();

        # First sentinel image after detection
        img_after = sentinel1\
            .filterDate(detection_dt.advance(1, 'day'), end_date)\
            .filterBounds(centroid).first()\
            .select('VH')

        date_SAR_after = img_after.date().format('YYYY-MM-dd').getInfo();

        # Define SQUARE bounding box around centroid point
        # add random x,y displacement in order not to bias the network to find deforestation in the center
        scale = 10
        projection = img_before.projection().crs()
        point = centroid.transform(projection).coordinates()
        centroid_displacement_x = 300*np.random.randn()
        centroid_displacement_y = 300*np.random.randn()
        xDistance = ee.Number(112).multiply(scale)
        yDistance = ee.Number(112).multiply(scale)
        rect = makeRectangle(point, xDistance, yDistance, centroid_displacement_x, centroid_displacement_y, projection)

        # CROP SAR image with bounding box
        img_before_clip = img_before.clip(rect)
        img_after_clip = img_after.clip(rect)
        #
        # generate target image
        fc = warnings.filterBounds(rect)\
            .filterMetadata('detection_dt_ms', 'greater_than', img_before.date().millis())\
            .filterMetadata('detection_dt_ms', 'less_than', img_after.date().millis())

        fc_image = ee.Image.constant(0).clip(fc.geometry()).mask()
        mask_image = img_before_clip.where(img_before_clip.lte(10), 0)
        target_image = mask_image.where(fc_image.eq(1), 1)

        # EXPORT
        task1=ee.batch.Export.image.toDrive(image= img_before, description= id + '_' + 'SAR_before_' + date_SAR_before,
                                            dimensions= '224x224', region= rect,
                                            folder= 'AI4Good_Project3_TeamA/03_Data/SAR_Input_mapbiomas')
        task1.start()

        task2 = ee.batch.Export.image.toDrive(image=img_after, description= id + '_' + 'SAR_after_' + date_SAR_after,
                                              dimensions='224x224',region=rect,
                                              folder= 'AI4Good_Project3_TeamA/03_Data/SAR_Input_mapbiomas');
        task2.start()

        task3 = ee.batch.Export.image.toDrive(image=target_image, description=id + '_'+'target_image_',
                                              dimensions= '224x224',region= rect,
                                              folder= 'AI4Good_Project3_TeamA/03_Data/SAR_Input_mapbiomas')
        task3.start()
    except BaseException as error:
            print('An exception occurred: {}'.format(error))
            continue
