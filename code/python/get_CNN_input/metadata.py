# IMPORTS
from __future__ import print_function
import datetime
import ee
import os
import math
from datetime import date, datetime, timedelta
import numpy as np

ee.Initialize()

# IMPORTS
sentinel1 = ee.ImageCollection('COPERNICUS/S1_GRD')
warnings = ee.FeatureCollection('users/juandb/DETER_SAR_EXP/mapbiomas_alerts_allattribs_2019_AMAZONIA')
prodes_2008_to_2018 = ee.FeatureCollection('users/tabeadonauer/yearly_deforestation_biome')
prodes_1990_2008 = ee.FeatureCollection('users/tabeadonauer/accumulated_deforestation_1988_2007_biome')


# set detection date in milliseconds for mapbiomas features
def set_data_detec_ms(ft):
    ft = ft.set('detection_dt_ms', ee.Date(ft.get('DataDetec')).millis())
    return ft

def get_metadata(feature):
    # get centroid of polygon

#
warnings = warnings.map(set_data_detec_ms).filterMetadata('AnoDetec', 'equals', 2019)\


# def get_metadata(feature):
#     # get centroid of polygon
#     centroid = feature.geometry().centroid()
#     id = str(feature.get('id').getInfo())
#     print(id)
#     # # SENTINEL IMAGES - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - -
#     start_date = '2018-12-15';
#     end_date = '2020-01-15';
#     detection_dt = ee.Date(feature.get('DataDetec'))
#
#     # Last image before detection
#     img_before = sentinel1.filterDate(start_date, detection_dt.advance(-1, 'day')) \
#         .filterBounds(centroid) \
#         .limit(1, 'system:time_start', False).first() \
#     date_SAR_before = img_before.date().format('YYYY-MM-dd').getInfo();
#
#     # First sentinel image after detection
#     img_after = sentinel1 \
#         .filterDate(detection_dt.advance(1, 'day'), end_date) \
#         .filterBounds(centroid).first() \
#
#     date_SAR_after = img_after.date().format('YYYY-MM-dd').getInfo();
#
#     # CROP SAR image with bounding box
#     img_before_clip = img_before.clip(rect)
#     img_after_clip = img_after.clip(rect)
#     mask_image = img_before_clip.where(img_before_clip.lte(10), 0)
#
#     feature = feature.set(SAR_date_1_before=date_SAR_1_before,
#                           SAR_date_1_after=date_SAR_1_after,
#                           SAR_date_2_before=date_SAR_2_before,
#                           SAR_date_2_after=date_SAR_2_after)
#
#
#     })
#
