# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 17:13:46 2020

@author: juand
"""
import ee
import logging
from sar_ee_utils import getS1dataFloat, toGamma0natural, refinedLeeFilter, makeMedianFilter, QueganYuFilter
from precipitation import get_precipitation
from timeit import default_timer as timer


def get_point_timeseries_2stab_filter(pol, ee_point, ee_date_detect, generalScale=20, detectPeriodMonths=3, learningPeriodYears=3, include_S1B=0):
    """
    Steps taken in this method:
    1. Get S1 timeseries collection from EE
    2. Obtain forest mask --> Masking all images in AOI, leaving only the forested pixels unmasked
    3. To stabilize SAR data TS, a harmonic as well as a spatial fitting approach are applied
       to result in two different fitted TS.
    4. Remove speckle noise by applying three different filters:
       Median, QueganYu (spatio-temporal), and Refined Lee (spatial)

    Args:
        pol (str): polarization VH or VV
        ee_point (ee.Geometry): Coordinates of points of which we want to obtain a timeseries
        ee_date_detect (ee.Date): Date of the start of SAR detection
        yearly_mask (str): Not sure...
        optical_mask (str): Current rasterized mask of DETER
        sar_mask (str): Accumulated detections of DETERSAR
        sar_tmp_mask(ee.FeatureCollection): Feature Collection of the DETERSAR CR2 polygons detected during the current run
        detection_threshold: NOT USED (used for detection, which is done by R scripts)
        generalScale (int): scale of the region we want to look at (pixel resolution in meters)
        forestBuffer (int): how large the forestBuffer should be
        searchDistance (int): buffer to be used for invariant forrest sampling spaces (in meters)
        detectPeriodMonths (int): I believe this is how many months it should take to detect deforestation
        learningPeriodYears (int): How many years the time series data should be for training
        stabilize (int/bool): NEVER USED
        stabilize_scale (int/bool): NEVER USED
        shrink (int/bool): NEVER USED
        include_S1B (int/bool): Whether to include data from Sentinel-1B
        adaptative_threshold: NEVER USED

    Returns: LIA + 6 different timeseries:
        LIA (Local Incident Angle),
        original_time_series (original Sentinel backscattered samples),
        original_time_series_f (original TS filtered),
        stabilized_time_series2 (original TS stabilized using harmonic fitting),
        stabilized_time_series3 (original TS stabilized using harmonic fitting and then filtered),
        stabilized_time_series2_f (original TS stabilized using spatial stabilization),
        stabilized_time_series3_f (original TS stabilized using spatial stabilization and then filtered)

    """
    logging.getLogger('googleapicliet.discovery_cache').setLevel(logging.ERROR)
    AOI = ee_point.buffer(10000)
    # Define relevant dates
    date2 = ee_date_detect
    date3 = date2.advance(detectPeriodMonths, 'months')  # Begin of detection (inclusive)
    date1 = date2.advance(-1 * learningPeriodYears, 'years')  # Begin of training colection (inclusive)
    date1str = date1.format('YYYY-MM-dd').getInfo()
    date2str = date2.format('YYYY-MM-dd').getInfo()
    date3str = date3.format('YYYY-MM-dd').getInfo()
    # First status messages
    print("Processing point")
    print("Learning period start: " + date1str)
    print("Detection start: " + date2str)
    print("Detection end: " + date3str)

    # Define S1 colection from EE
    # insert here .map(get_precipitation)
    colS1 = getS1dataFloat(date1, date3).filterBounds(AOI) \
        .sort('system:time_start') \
        .map(get_precipitation(ee_point)) \
        .map(lambda img: img.updateMask(img.gt(.00002))) \
        .map(toGamma0natural) \
        .select(["VHg0", "VVg0", "LIA"])
    LIA = colS1.select(["LIA"]).mean().reduceRegion('mean', ee_point, generalScale).getInfo().get("LIA")
    print("Local Incidence Angle: {}".format(LIA))
    if not include_S1B:
        colS1 = colS1.filterMetadata("platform_number", "equals", "A")

    # Leave only the most represented orbit in every pixel
    # add relative orbit number
    def addOrbitBand(img):
        orb = ee.Number(img.get('relativeOrbitNumber_start'))
        return img.addBands(ee.Image(orb).rename('Orbit').toInt())

    colS1 = colS1.map(addOrbitBand)
    orbit_mode = colS1.select('Orbit').reduce(ee.Reducer.mode())
    colS1 = colS1.map(lambda img: img.updateMask(img.select('Orbit').eq(orbit_mode))).select(["VHg0", "VVg0", "LIA"])
    colSize = colS1.size().getInfo()
    print("# of collected images: " + str(colSize))
    if colSize > 0:
        #original_time_series = colS1.select(pol).toBands().reduceRegion('mean', ee_point, 20)

        def add_precipitation_bands(image):
            return image.addBands(image.metadata("p_12_hours_mm").addBands(image.metadata("p_60_days_mm")))
        colS1_meteo = colS1.map(add_precipitation_bands)
        precipitation_12h_time_series = colS1_meteo.select("p_12_hours_mm").toBands().reduceRegion('mean', ee_point, 20)
        precipitation_60d_time_series = colS1_meteo.select("p_60_days_mm").toBands().reduceRegion('mean', ee_point, 20)
        # Speckle filtering
        median5 = makeMedianFilter(5)
        colS1_f = QueganYuFilter(colS1.select(pol), median5).map(refinedLeeFilter)
        original_time_series_f = colS1_f.toBands().reduceRegion('mean', ee_point, 20)
    else:
        original_time_series_f = \
        precipitation_12h_time_series = \
        precipitation_60d_time_series =  None
    return LIA, original_time_series_f, precipitation_12h_time_series, precipitation_60d_time_series 
