# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 09:38:21 2020

@author: juand
"""
import ee,datetime

#needed for spatial stabilizations afterwards
def get_forest_mask(date_start_detection,yearly_mask,optical_mask,sar_mask,sar_tmp_mask):
    """
    Returns a forest mask, computed as the negation of the addition of all available rasterized non-forest masks:
        - Acummulated 2019 deforestation (PRODES/INPE), hidrology (INPE), non forest (INPE) and varzea mask (INPE)
        - Ongoing optical deforestation warnings (DETER), until date_start_detection
        - DETERSAR CR2 warnings until the actual run
        - DETERSAR_CR2 warnings computed on the actual run 
    Args:
        date_start_detection (ee.Date): Date of the start of SAR detection. Used to mask out any posterior optical deforestation data.
        yearly_mask
        optical_mask (STR): Current rasterized mask of DETER. Should have unique band representing the number of elapsed days from 2018-01-01.
                            Use 'precompute_deforest_mask' to compute this image from DETER polygons.
        sar_mask (STR):    Accumulated detections of DETERSAR
        sar_tmp_mask (ee.FeatureCollection): Feature Collection of the CR2 polygons detected during the current run
    Returns:
        ee.Image: mask representing forested pixels.

    """
    #make 1smask from yearly_mask, set invalid pixels to 0 (?)
    deforestationMaskRaster=make1sFromMask(ee.Image(yearly_mask)).unmask(0,False)
    #Amount of days since 1jan2018 untill date_start_detection
    days_from_1jan2018=date_start_detection.difference(ee.Date('2018-01-01'),'day')

    #if no optical_mask, set deforestationMaskRaster_DETER to constant image, else to 1 for each matched pair in the bands of optimal_mask and Image(days_from_1jan2018) when optimal_mask < Image(days_from_1jan2018)
    if optical_mask==None:
        deforestationMaskRaster_DETER=ee.Image(0)
    else:
        deforestationMaskRaster_DETER=ee.Image(optical_mask).lt(ee.Image(days_from_1jan2018)).unmask(0,False)

     #If no sar_mask, deforestationMaskRaster_DETERSAR empty image, else get 1s mask from sar_mask
    if sar_mask==None:
        deforestationMaskRaster_DETERSAR=ee.Image(0)
    else:
        deforestationMaskRaster_DETERSAR=make1sFromMask(ee.Image(sar_mask)).unmask(0,False)


    #reduce feature collection to image
    deforestationMaskRaster_DETERSAR_TMP=ee.Image(sar_tmp_mask.map(lambda ft:ft.set('desm',1)).reduceToImage(['desm'],'first')).unmask(0,False)
    #"Or" everything together to have 1 everwhere where deforestation had been observed in at least one of the datasets
    deforestationMask=deforestationMaskRaster.Or(deforestationMaskRaster_DETER).Or(deforestationMaskRaster_DETERSAR).Or(deforestationMaskRaster_DETERSAR_TMP)

    #return negation of deforestationMask
    return deforestationMask.Not()

#Creating mask with 1s where masked
def make1sFromMask(img):
    return img.selfMask().add(1).divide(img.add(1))

#returns a precomputed deforestation mask out of the detersar warnings
def precompute_deforest_mask(warning_vector_asset="users/juandb/DETER_SAR_PRODUCAO/deter-amz-public-2020Mai26",output_asset="users/juandb/DETER_SAR_PRODUCAO/deter_amz_public_2020Mai26_CR_raster"):
    DETER_CR = ee.FeatureCollection(warning_vector_asset) \
                .filterMetadata("CLASSNAME","equals","DESMATAMENTO_CR")
    DETER_CR_VEG = ee.FeatureCollection(warning_vector_asset) \
                .filterMetadata("CLASSNAME","equals","DESMATAMENTO_VEG")

    def add_date(ft):return ft.set("days_from_1jan2018",ee.Date(ft.get("VIEW_DATE")).difference(ee.Date('2018-01-01'),'day'))
    #Take only data older than 212 days                  
    DETER=DETER_CR.merge(DETER_CR_VEG).map(add_date).filterMetadata("days_from_1jan2018","greater_than",212)
    print (DETER.size().getInfo())
    #reduce the imported data to an image by taking mean on the input
    deforestationMaskRaster_DETER=DETER.reduceToImage(["days_from_1jan2018"],ee.Reducer.mean()).rename('days_from_2018').toInt16()
    print(deforestationMaskRaster_DETER.getInfo())
    #export image
    task=ee.batch.Export.image.toAsset(image=deforestationMaskRaster_DETER,description="export_forest_mask",assetId=output_asset,scale=20,maxPixels=3400123231231)
    return task


#
def rasterize_deforestation_polygons(pols_asset="users/juandb/DETER_SAR_PRODUCAO/DETERSAR_CR2_TRANSMITIDOS_4AREAS_ACUMULADO",output_asset="users/juandb/DETER_SAR_PRODUCAO/DETERSAR_CR2_TRANSMITIDOS_4AREAS_ACUMULADO_raster"):
    #mask to image
    raster=ee.FeatureCollection(pols_asset).map(lambda ft:ft.set('desm',1)).reduceToImage(['desm'],'first')
    #export image
    task=ee.batch.Export.image.toAsset(image=raster,description="rasterize",assetId=output_asset,scale=20,maxPixels=3400123231231)
    return task

if __name__== '__main__':
    task16=rasterize_deforestation_polygons()
    
