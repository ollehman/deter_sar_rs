# -*- coding: utf-8 -*-
"""
Created on Oct 21, 2021

@author: Tabea Donauer
"""
import ee
import datetime
import pandas
import math
import logging
from timeit import default_timer as timer

def get_precipitation(ee_point):
    """
    Steps taken in this nested function:
    1. get SAR image date
    2. compute time period from which to get precipitation
    3. import GSMaP global hourly precipitation product, 10 km spatial resolution
    4. set properties of the SAR image (12 hours and 60 days precipitation)

    Args:  image
    Returns: image
    """
    def wrap(S1_img):

        end_date = S1_img.date()
        start_date_12_hours = end_date.advance(-12, 'hour')
        start_date_60_days = end_date.advance(-60, 'day')

        GSMaP_operational = ee.ImageCollection("JAXA/GPM_L3/GSMaP/v6/operational")

        p_12_hours = GSMaP_operational\
            .filterDate(start_date_12_hours, end_date)\
            .select('hourlyPrecipRateGC') \
            .reduce(reducer=ee.Reducer.sum())\
            .reduceRegion(reducer=ee.Reducer.mean(), geometry = ee_point, scale = 1000)\
            .get('hourlyPrecipRateGC_sum')

        p_60_days = GSMaP_operational\
            .filterDate(start_date_60_days, end_date)\
            .select('hourlyPrecipRateGC') \
            .reduce(reducer=ee.Reducer.sum()) \
            .reduceRegion(reducer=ee.Reducer.mean(), geometry=ee_point, scale=1000) \
            .get('hourlyPrecipRateGC_sum')

        return S1_img.set({'p_12_hours_mm': p_12_hours, 'p_60_days_mm': p_60_days})

    return wrap