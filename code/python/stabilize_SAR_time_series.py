# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 10:27:50 2020

@author: juand
"""

import ee,math

def stabilize_SAR_meteorological_data(collection, date1, date2, pol):
  precipitation_labels = ee.List(["p_12_hours_mm", "p_60_days_mm"])
  
  def add_precipitation_bands(image):
    return image.addBands(image.metadata("p_12_hours_mm").addBands(image.metadata("p_60_days_mm")))
  colS1_meteo = collection.map(add_precipitation_bands)

  # Fit meteorological data to SAR data by Linear Regression
  meteorolgicalLR = colS1_meteo.filterDate(date1,date2)\
            .select(precipitation_labels.add(pol))\
            .reduce(ee.Reducer.linearRegression(2, 1)) 
  meteorolgicalLRCoefficients = meteorolgicalLR.select("coefficients") \
            .arrayProject([0]) \
            .arrayFlatten([precipitation_labels])

  def compute_fitted_meteorological(image):
    fit = image.select(precipitation_labels) \
            .multiply(meteorolgicalLRCoefficients) \
            .reduce("sum") \
            .rename("fitted")
    return image.addBands(fit) \
            .addBands(image.select(pol).subtract(fit).rename("residual"))

  # Compute the residuals and create new fitted TS
  fittedMeteorological = colS1_meteo.map(compute_fitted_meteorological)
  return fittedMeteorological

#stabilizes collection of images, mask should be the forestmask computed by get_forest_mask()
def stabilize_SAR_time_series (collection, mask, search_distance_meters):
    # collection should have only one band of linear scale backscattering
    #add a band with the forest_mean
    def compute_forest_mean(img):
      #applies the reducer on the neighborhood of each pixel, where the kernel defines the neigborhood
        forest_mean=img.updateMask(mask).reduceNeighborhood(
          reducer = ee.Reducer.mean(),
          kernel = ee.Kernel.square(search_distance_meters,"meters",False),
          inputWeight = "mask",
          skipMasked = False,
          optimization = "boxcar"
          ).copyProperties(img,["system:time_start"])
        return img.addBands(ee.Image(forest_mean))
    
    colS1Forest=collection.map(compute_forest_mean)

    #add a band to the image with a coefficent which says how much the forest_mean differs from the original image (1 if same)
    def compute_coef(img):
      #select band 1 of image, divide through mean of the forest_mean, unmask it, copy properties from image
      return img.addBands(img.select(1).divide(colS1Forest.select(1).mean()).unmask(1).copyProperties(img,["system:time_start"]))
    colS1Forest=colS1Forest.map(compute_coef)

    #divides band 0 through band 2 of img    
    def compute_corrected_backscattering(img):
      return img.select(0).divide(img.select(2)).copyProperties(img,["system:time_start"])
    #here band 0 will be the original image and band 2 the coefficent  
    return colS1Forest.map(compute_corrected_backscattering)


#does the same as the function before, but also upscales the images in the def of computer_forest_mean, rest is the same
def stabilize_SAR_time_series_upscale (collection, mask, search_distance_meters,scale):
    # collection should have only one band of linear scale backscattering
    #instead of copying the properties of img in the end it forces the img to be computed in the given resolution
    def compute_forest_mean(img):
          forest_mean=img.updateMask(mask).reduceNeighborhood(
          reducer = ee.Reducer.mean(),
          kernel = ee.Kernel.square(search_distance_meters,"meters",False),
          inputWeight = "mask",
          skipMasked = False,
          optimization = "boxcar"
          ).reproject(crs=img.projection(),scale=scale).copyProperties(img,["system:time_start"])
          #).reduceResolution('mean', True, 64).reproject(crs=img.projection(),scale=scale).copyProperties(img,["system:time_start"])
          return img.addBands(ee.Image(forest_mean))
    colS1Forest=collection.map(compute_forest_mean)
    def compute_coef(img):
      return img.addBands(img.select(1).divide(colS1Forest.select(1).mean()).unmask(1).copyProperties(img,["system:time_start"]))
    colS1Forest=colS1Forest.map(compute_coef)
       
    def compute_corrected_backscattering(img):
      return img.select(0).divide(img.select(2)).copyProperties(img,["system:time_start"])
    return colS1Forest.map(compute_corrected_backscattering)

#TODO
#input: collection is the img collection, date1 is, date2 is, pol is, returns 
def stabilize_SAR_time_series_harmonic (collection,date1,date2,pol):
    #adding the difference in years between date1 and the imagedate as a band to the image
    def addVariables(image):
        years = image.date().difference(date1, 'year')
        return image.addBands(ee.Image(years).rename('t')).addBands(ee.Image.constant(1)).float()
    colS1_st2 = collection.map(addVariables)
    #harmonicIndependents = ee.List(['constant', 't', 'cos', 'sin']) # Won't use trend
    harmonicIndependents = ee.List(['constant', 'cos', 'sin'])
    dependent = pol
    def addHarmonizedVars(image):
        timeRadians = image.select('t').multiply(2 * math.pi)
        return image.addBands(timeRadians.cos().rename('cos')).addBands(timeRadians.sin().rename('sin'))
    harmonicsS1=colS1_st2.map(addHarmonizedVars)
    harmonicLR = harmonicsS1.filterDate(date1,date2)\
        .select(harmonicIndependents.add(dependent))\
        .reduce(ee.Reducer.linearRegression(harmonicIndependents.length(), 1))
    # Turn the array image into a multi-band image of coefficients.
    harmonicLRCoefficients = harmonicLR.select('coefficients')\
        .arrayProject([0])\
        .arrayFlatten([harmonicIndependents])
    def computeFittedHarmonicNoIntercept(image):
        # fit = image.select(harmonicIndependents)\
        #         .multiply(harmonicLRCoefficients)\
        fit = image.select(['cos','sin'])\
                .multiply(harmonicLRCoefficients.select(['cos','sin']))\
                .reduce('sum')\
                .rename('fitted')
        return image.addBands(fit)\
          .addBands(image.select(pol).subtract(fit).rename("residual"))
    fittedHarmonic=harmonicsS1.map(computeFittedHarmonicNoIntercept)
    return fittedHarmonic.select("residual")
    
