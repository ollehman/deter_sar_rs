from datetime import datetime, time, timedelta
from os import times

def get_timestamps_from_name_string(datapoint_names):
    timestamps = [dp.split("_")[4] for dp in datapoint_names]
    #datetime_format = '%Y%m%dT%H%M%S'
    #timestamps = [datetime.strptime(ts, datetime_format) for ts in timestamps]
    return timestamps


def format_data(j, LIA, point, ee_begin_detect_date, datapoint_names, timeseries, is_forest):
    #NDVI_before = point.get('NDVI_before').getInfo()
    #NDVI_after = point.get('NDVI_after').getInfo()
    NDVI_before = None
    NDVI_after = None
    datetime_format = '%Y-%m-%d %H:%M:%S'
    before_dt = datetime.fromtimestamp(ee_begin_detect_date.millis().getInfo()//1000)
    if is_forest:
        detection_dt = before_dt + timedelta(days=5)
        after_dt = before_dt + timedelta(days=40)
    else:
        detection_dt = point.get('detection_dt').getInfo()
        detection_dt = datetime.fromtimestamp(detection_dt//1000)
        after_dt = point.get('after_dt').getInfo()
        after_dt = datetime.fromtimestamp(after_dt//1000)
    before_dt = before_dt.strftime(datetime_format)
    detection_dt = detection_dt.strftime(datetime_format)
    after_dt = after_dt.strftime(datetime_format)

    timestamps = get_timestamps_from_name_string(datapoint_names)
    orig_f = {ts: sar_value for ts, sar_value in zip(timestamps, timeseries)}
    
    data = {
        "deforested": not is_forest,
        "LIA": LIA,
        "cooridnates": point.geometry().coordinates().getInfo(),
        "NDVI_before": NDVI_before,
        "NDVI_after": NDVI_after,
        "before_dt": before_dt,
        "detection_dt": detection_dt,
        "after_dt": after_dt,
        "orig_f": orig_f,
    }
    return data